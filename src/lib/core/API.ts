const base = "https://mc.schindlerfelix.de";

export async function GET(endpoint: string): Promise<any> {
	const res = await fetch(base + endpoint);
	return await res.json();
}

export async function POST(endpoint: string, data: any): Promise<any> {
	const res = await fetch(base + endpoint, {
		method: "POST",
		body: JSON.stringify(data),
	});
	return await res.json();
}
