export type API_Sale = {
	id: number;
	player: string;
	playerName: string;
	item: string;
	quantity: number;
	price: number;
	itemMeta: string;
};
