import type { Writable } from "svelte/store";
import { writable as ls_writable } from "svelte-local-storage-store";

export const authStore: Writable<AuthStore | null> = ls_writable("auth", null);

export class AuthStore {
	public readonly uuid: string; // 7bbecec4-56ca-4f8e-8de5-019a8cba020d
	public readonly name: string;	// FelixDev
	public balance: number;
	public readonly token: string;

	public constructor(uuid: string, name: string, balance: number, token: string) {
		this.uuid = uuid;
		this.name = name;
		this.balance = balance;
		this.token = token;
	}

	get isValid() {
		const decode = atob(this.token);
		const validUntil = atob(decode.split(".")[2]);
		return (new Date() > new Date(validUntil));
	}
};
