/**
 * Takes the given URL and tries to fetch the favicon of the site.
 *
 * @param req Request of the user
 * @returns Either the favicon of the website or HTTP-400, when failed
 */
export async function GET(req: any): Promise<Response> {
	try {
		const res = await fetch(`https://cravatar.eu/helmavatar/${req.params.username}/64.png`);
		const blob = await res.blob();

		return new Response(blob, { headers: { "Content-Length": String(blob.size), "Content-Type": blob.type, "Cache-Control": "max-age=604800" } });
	} catch {
		return new Response("Failed to get favicon", { status: 400, statusText: "Failed to get favicon", headers: { "Cache-Control": "max-age=300" } });
	}
};
